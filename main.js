// Отримуємо посилання на DOM-елементи
const previousBtn = document.getElementById('previousBtn');
const nextBtn = document.getElementById('nextBtn');
const slides = document.querySelectorAll('.slider-container img');

let currentIndex = 0;

// Функція для переключення до наступного слайда
function goToNextSlide() {
  slides[currentIndex].classList.remove('active');
  currentIndex = (currentIndex + 1) % slides.length;
  slides[currentIndex].classList.add('active');
}

// Функція для переключення до попереднього слайда
function goToPreviousSlide() {
  slides[currentIndex].classList.remove('active');
  currentIndex = (currentIndex - 1 + slides.length) % slides.length;
  slides[currentIndex].classList.add('active');
}

// Обробники подій для кнопок
nextBtn.addEventListener('click', goToNextSlide);
previousBtn.addEventListener('click', goToPreviousSlide);